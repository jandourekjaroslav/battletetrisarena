unit GameObjects;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,Graphics;
type Tstance = (A, D, H, M);

  const
    WIDTH = 9;
    HEIGHT = 22;
    chunkwh=3;
    CHUNK_WIDTH = 3;
    CHUNK_HEIGHT = 3;

type
  TPlayer = class (TObject)
  Name : string;
  MaxHP : integer;
  DiffBl:integer;
  DiffBlN:integer;
  linedes:integer;
  Attack:integer;
  Block:integer;
  spellgo:boolean;
  CurrentHP : integer;
  MaxMa : integer;
  CurrentMa : integer;
  ADR : integer;
  ArmorR : integer;
  HealR : integer;
  Stance : TStance;
  StanceCaption : string;
  Skip: TStance;
  avt:integer;
  sickness:integer;
  FieldBoard : array [1..WIDTH, 1..HEIGHT] of boolean;
  FieldCHUNK : array [1..CHUNK_WIDTH, 1..CHUNK_HEIGHT] of boolean;
  fieldpchunk: array [1..CHUNK_WIDTH, 1..CHUNK_HEIGHT] of boolean;
  FieldNextCHUNK : array [1..CHUNK_WIDTH, 1..CHUNK_HEIGHT] of boolean;
  ChunkShiftX : Integer;
  ChunkShiftY : Integer;
  ChunkStyle: Integer;
  ChunkStyleN: Integer;
  Linecount:integer;
  obr1:TPicture;
  NEXTBLOCK: INTEGER;
  nabbblock:integer;
  public
  constructor create;
  function GetHeight : integer;
  function GetWidth : integer;
  function Getchunkwh:Integer;
  function CanMoveTo (a,b : integer) : boolean;
  function CanTocleftTo  : boolean;
  function CanTocRightTo  : boolean;
  procedure initialize (avatar : integer);
  procedure CopyChunkToBoard;
  procedure ChangeStance;
  procedure Rename(avatar : integer);
  destructor ruin;
  end;

implementation
constructor TPlayer.create;

begin
 spellgo:=false;


 ChunkStyleN :=1;               //zde se priradi prvni chunk, aby mohl zacit cyklus//
      FieldpChunk[3, 2] := True;
      FieldpChunk[3, 3] := True;
      FieldpChunk[2, 2] := True;
      FieldpChunk[2, 3] := True;
      diffblN := 1;
FieldBoard[1,1] := true;          //v teto casti se vyplni postranni rady, aby chunk nemohl byt vysunut z pole//
FieldBoard[2,1] := true;
FieldBoard[3,1] := true;
FieldBoard[4,1] := true;
FieldBoard[5,1] := true;
FieldBoard[6,1] := true;
FieldBoard[7,1] := true;
FieldBoard[8,1] := true;
FieldBoard[9,1] := true;



FieldBoard[1,1] := true;
FieldBoard[1,2] := true;
FieldBoard[1,3] := true;
FieldBoard[1,4] := true;
FieldBoard[1,5] := true;
FieldBoard[1,6] := true;
FieldBoard[1,7] := true;
FieldBoard[1,8] := true;
FieldBoard[1,9] := true;
FieldBoard[1,10] := true;
FieldBoard[1,11] := true;
FieldBoard[1,12] := true;
FieldBoard[1,13] := true;
FieldBoard[1,14] := true;
FieldBoard[1,15] := true;
FieldBoard[1,16] := true;
FieldBoard[1,17] := true;
FieldBoard[1,18] := true;
FieldBoard[1,19] := true;
FieldBoard[1,20] := true;
FieldBoard[1,21] := true;
FieldBoard[1,22] := true;

FieldBoard[9,1] := true;
FieldBoard[9,2] := true;
FieldBoard[9,3] := true;
FieldBoard[9,4] := true;
FieldBoard[9,5] := true;
FieldBoard[9,6] := true;
FieldBoard[9,7] := true;
FieldBoard[9,8] := true;
FieldBoard[9,9] := true;
FieldBoard[9,10] := true;
FieldBoard[9,11] := true;
FieldBoard[9,12] := true;
FieldBoard[9,13] := true;
FieldBoard[9,14] := true;
FieldBoard[9,15] := true;
FieldBoard[9,16] := true;
FieldBoard[9,17] := true;
FieldBoard[9,18] := true;
FieldBoard[9,19] := true;
FieldBoard[9,20] := true;
FieldBoard[9,21] := true;
FieldBoard[9,22] := true;

FieldBoard[1,22] := true;
FieldBoard[2,22] := true;
FieldBoard[3,22] := true;
FieldBoard[4,22] := true;
FieldBoard[5,22] := true;
FieldBoard[6,22] := true;
FieldBoard[7,22] := true;
FieldBoard[8,22] := true;
FieldBoard[9,22] := true;

//FieldBoard[Width,Height] := true;
//FieldBoard[Width-1,Height] := true;
//FieldBoard[Width-2,Height] := true;
//FieldBoard[Width-1,Height-1] := true;
//t1padTimer.Enabled:=true;
Linecount:=0;
end;

destructor TPlayer.ruin;
begin

end;

procedure TPlayer.initialize (avatar : integer);

 begin
 randomize;
 sickness:=0;
 Nextblock := random (5);
     case avatar of                       //zde se vyplnuji promenne podle  vybraneho avatara
     1:
       //adc
       begin
       MaxHP := 200;
      CurrentHP := 200;
    avt:=1;
    Skip := M;  //schopnost, ktera avatarovi chybi
    Stance := A;
    ADR:=11;
    ArmorR:=8;
    HealR:=-6;
    block:=0;
    Name := 'Oberyn Martell, the Red Viper';

       end;
     2:
       begin
      MaxHP := 250;
     CurrentHP := 250;
     avt:=2;
     Skip := M;
     Stance := A;
      ADR:=9;
    ArmorR:=8;
    HealR:=-8;
    block:=0;
    Name := 'Garen, Might of Demacia';
     //Tank
       end;
     3:
       begin
      MaxHP := 220;
     CurrentHP := 220;
     avt:=3;
     Skip := D;
     Stance := A;
      ADR:=9;
    ArmorR:=1;
    HealR:=8;
    block:=0;
    Name := 'Tyrande Whisperwind';
      //heal
       end;
     4:
       begin
       MaxHP := 210;
     CurrentHP := 210;
     avt:=4;
     Skip := D;
     Stance := A;
     ADR:=7;
     ArmorR:=1;
     HealR:=5;
     block:=0;
     Name := 'Jace Beleren, the Mind-Sculptor';
     //Magic
       end;
     end;



  end;

procedure TPlayer.rename (avatar : integer);
 begin
 case avatar of
 1:  Name := 'Prince Nuada';
 2:  Name := 'Gregor Clegane, the Mountain';
 3:  Name := 'Nissa Revane';
 4:  Name := 'Eldrad Ulthran';
 end;
 end;

 function TPlayer.GetHeight : integer;
 begin
 result := HEIGHT-1;
 end;

 function TPlayer.GetWidth : integer;
 begin
 result := WIDTH-1;
 end;
 function TPlayer.Getchunkwh:Integer;
 begin
 result:=chunkwh;
 end;

function TPlayer.CanMoveTo(a, b : integer) : boolean;     //zjistuje, jestli se blok muze posunout na danou souradnici, bude volano na tlacitkach//
var x,y : integer;


begin
  result := true;

  for x := 1 to CHUNK_WIDTH do begin
  for  y := 1 to CHUNK_HEIGHT do begin
  if (FieldCHUNK [x,y] = true) and (FieldBoard[(x + (a-1)),(y + (b-1))]= true) then result := false;
  end;
end;
  end;

function TPlayer.CanTocleftTo : boolean;          //podobne kontroluje, jestli se lze otocit doleva//
var x,y : integer;


begin
  result := true;

  for x := 1 to CHUNK_WIDTH do begin
  for  y := 1 to CHUNK_HEIGHT do begin
  if (FieldCHUNK [x,y] = true) and (FieldBoard[y + ChunkshiftX, (CHUNK_HEIGHT - x +1+ ChunkshiftY)]= true) then result := false;
  end;
end;
  end;
function TPlayer.CanTocrightTo : boolean;          //analogicky doprava//
var x,y : integer;


begin
  result := true;
  //TODO: check if not out of FieldBoard bounds
  for x := 1 to CHUNK_WIDTH do begin
  for  y := 1 to CHUNK_HEIGHT do begin
  if (FieldCHUNK [x,y] = true) and (FieldBoard[(CHUNK_HEIGHT - y +1)+ ChunkshiftX, x+ChunkshiftY ]= true) then result := false;
  end;
end;
  end;
procedure TPlayer.CopyChunkToBoard;                  //prepisuje chunk do herniho pole, vola se, kdyz se chunk nemuze dal posunout dolu//
 var x, y: integer;
 begin
   for x := 1 to CHUNK_WIDTH do begin
   for  y := 1 to CHUNK_HEIGHT do begin
   if FieldCHUNK [x,y] = true then  FieldBoard[(x + ChunkShiftX),(y + ChunkShiftY)] := true;
   end;
 end;

   end;
procedure TPlayer.ChangeStance;                   //meni aktivni schopnost, je to kruh ve smeru a-d-h-m-a, tady by se byval dal pouzit spojovy seznam//
 begin
  repeat

  case Stance of
  A: Stance := D;
  D: Stance := H;
  H: Stance := M;
  M: Stance := A;
  END;
  UNTIL sTANCE <> SKIP;
  end;




 end.


