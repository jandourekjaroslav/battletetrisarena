unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  StdCtrls, ExtCtrls, Unit2, OvladaniGameForm;

type

  { TCharSelection }

  TCharSelection = class(TForm)
    Button1: TButton;
    adc1: TImage;
    tank1: TImage;
    heal1: TImage;
    mage1: TImage;
    adc2: TImage;
    tank2: TImage;
    heal2: TImage;
    mag2: TImage;
    backgrimg: TImage;
    MainMenu1: TMainMenu;
    MenuItem2: TMenuItem;
    adc1b: TRadioButton;
    Tank1b: TRadioButton;
    Heal1b: TRadioButton;
    mage1b: TRadioButton;
    adc2b: TRadioButton;
    tank2b: TRadioButton;
    heal2b: TRadioButton;
    Mage2b: TRadioButton;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    procedure Button1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  CharSelection: TCharSelection;

implementation

{$R *.lfm}

{ TCharSelection }

procedure TCharSelection.MenuItem2Click(Sender: TObject);

begin
  Self.hide;
  Tutorial.ShowModal;
  Self.show;
end;

procedure TCharSelection.RadioGroup1Click(Sender: TObject);
begin

end;

procedure TCharSelection.Button1Click(Sender: TObject);
begin
  self.hide;
  if adc1b.checked then Gameform.PL1AV := 1;
  if Tank1b.checked then Gameform.PL1AV := 2;
  if Heal1b.checked then Gameform.PL1AV := 3;
  if mage1b.checked then Gameform.PL1AV := 4;
  if adc2b.checked then Gameform.PL2AV := 1;
  if tank2b.checked then Gameform.PL2AV := 2;
  if heal2b.checked then Gameform.PL2AV := 3;
  if Mage2b.checked then Gameform.PL2AV := 4;
  GameForm.ShowModal;



end;

end.

