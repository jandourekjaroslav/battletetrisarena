program project1;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Unit1, Unit2, OvladaniGameForm, GameObjects;

{$R *.res}

begin
 // RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TCharSelection, CharSelection);
  Application.CreateForm(TTutorial, Tutorial);
  Application.CreateForm(TGameForm, GameForm);
  Application.Run;
end.

